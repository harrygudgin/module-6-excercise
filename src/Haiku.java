public class Haiku {

    public static void main(String[] args) {
        String[][] strings = {
                {"An", "Old", "Silent", "Pond..."},
                {"A", "frog", "jumps", "into", "the", "pond,"},
                {"splash!", "Silence", "again."}
        };

        for (String[] row : strings) {
            for (String column : row) {
                System.out.print(column + " ");
            }
            System.out.println();
        }
    }
}